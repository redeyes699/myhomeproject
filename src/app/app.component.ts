import { SignupDialogComponent } from './signup-dialog/signup-dialog.component';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { RouterModule } from '@angular/router';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public dialog: MatDialog,){}

  OpenDialog(){
    this.dialog.open(LoginDialogComponent);
  }

  OpenDialogreg(){
    this.dialog.open(SignupDialogComponent);
  }

}
