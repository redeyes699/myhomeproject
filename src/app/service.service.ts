import { Injectable } from '@angular/core';
import { EmailValidator } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor() { }

  Users: Array<{id: number, username : string, password: string, password2: string, email: string}> = 
  [
    {id : 1, username: 'shako', password: '1234' , password2: '1234', email:'s.surguladze95@gmail.com'}
  ];

  PsychoArray: Array<{title: string, text: string, avtori: string}> = [
    {title : 'წარმატების ფსიქოლოგია',
     text: `
     მუდმივად იფიქრეთ, რომ ყველაფერი შეგიძლიათ შეცვალოთ ისე, როგორც თქვენ გსურთ. თვითსრულყოფის პროცესს ხომ საზღვრები არა აქვს.

მნიშვნელოვანია სრულად აკონტროლოთ საკუთარი ემოციები და ეცადოთ მიეჩვიოთ რაციონალურად აზროვნებას. მხოლოდ ლოგიკურ აზროვნებაზე დაყრდნობით შეგიძლიათ გაერკვეთ და ჩამოგიყალიბდეთ აზრი გარე სამყაროზე.

შეეცადეთ შეცვალოთ უკეთესობისკენ ამ სამყაროში და საკუთარ თავში ის, რისი შეცვლაც გსურთ.

ნუ აჰყვებით ემოციებს, იფიქრეთ, იაზროვნეთ, სწორად შეაფასეთ მიმდინარე მოვლენები, ყოველი ღონე იხმარეთ, რათა მიიღოთ გონივრული გადაწყვეტილებები.
     `, 
     avtori: 'ნაპოლეონ ჰილი'}
  ];

  AddPost(title: string, text: string, avtori: string){
    this.PsychoArray.push({title,text, avtori})
    console.log(this.PsychoArray);
}

getText(){
  return this.PsychoArray;
}

}
