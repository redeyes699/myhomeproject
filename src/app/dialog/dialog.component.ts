import { ServiceService } from './../service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  constructor(private service: ServiceService) { }

  title : string;
  text : string
  avtori: string;
    AddPost(title: string, text: string, avtori: string){
      this.title = title;
      this.text = text;
      this.avtori = avtori;
      this.service.AddPost(this.title, this.text, this.avtori)
         
  
    }


  ngOnInit() {
  }

}
