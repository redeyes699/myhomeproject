import { SignupDialogComponent } from './signup-dialog/signup-dialog.component';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { DialogComponent } from './dialog/dialog.component';
import { KulinariaComponent } from './kulinaria/kulinaria.component';
import { PoliticsComponent } from './politics/politics.component';
import { HomePageComponent } from './home-page/home-page.component';
import { PsychoComponent } from './psycho/psycho.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'psycho', component: PsychoComponent},
  { path: 'politics', component: PoliticsComponent},
  { path: 'kulinaria', component: KulinariaComponent},
  { path: 'home', component: HomePageComponent},
  { path: 'dialog', component: DialogComponent},
  { path: 'logindialog', component: LoginDialogComponent},
  { path: 'signupdialog', component: SignupDialogComponent},
  {path: '',   redirectTo: '/home', pathMatch: 'full'},
]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }