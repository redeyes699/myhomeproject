import { ServiceService } from './../service.service';
import { DialogComponent } from './../dialog/dialog.component';
import { Component, OnInit } from '@angular/core';
import { stringify } from 'querystring';
import { MatDialog } from '@angular/material/dialog';



@Component({
  selector: 'app-psycho',
  templateUrl: './psycho.component.html',
  styleUrls: ['./psycho.component.scss']
})
export class PsychoComponent implements OnInit {

  constructor(public dialog: MatDialog, private ServiceService: ServiceService) { }


  PsychoArray: Array<{title: string, text: string, avtori: string}>;

  OpenDialog(){
    this.dialog.open(DialogComponent);
  }





  ngOnInit() {

   this.PsychoArray =  this.ServiceService.getText();

  }


 
}


